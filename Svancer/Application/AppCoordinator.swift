//
//  AppCoordinator.swift
//  Svancer
//
//  Created by Jan Švancer on 05/11/2019.
//  Copyright © 2019 jansvancer. All rights reserved.
//

import UIKit
import RxSwift

final class AppCoordinator: BaseCoordinator<Void> {

    private let window: UIWindow
    private let dependencies: AppDependency

    init(window: UIWindow, application: UIApplication) {
        self.window = window
        self.dependencies = AppDependency(application: application)
    }

    override func start() -> Observable<Void> {
        coordinateToRoot()
        return Observable.never()
    }

    // Recursive method that will restart a child coordinator after completion.
    // Based on:
    // https://github.com/uptechteam/Coordinator-MVVM-Rx-Example/issues/3
    private func coordinateToRoot() {
        showLogin()
            .subscribe(onNext: { [weak self] _ in
                self?.window.rootViewController = nil
                self?.coordinateToRoot()
            })
            .disposed(by: disposeBag)
    }

    private func showLogin() -> Observable<Void> {
        let coordinator = LoginCoordinator(window: window, dependencies: dependencies)
        return coordinate(to: coordinator)
    }

}
