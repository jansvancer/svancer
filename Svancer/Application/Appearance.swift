//
//  Appearance.swift
//  Svancer
//
//  Created by Jan Švancer on 05/11/2019.
//  Copyright © 2019 jansvancer. All rights reserved.
//

import UIKit

struct Appearance {

    // MARK: - Global

    static func setGlobalAppearance() {
        if #available(iOS 13.0, *) {
            let navigationBarAppearance = UINavigationBarAppearance().tap {
                $0.configureWithTransparentBackground()
                $0.titleTextAttributes = [
                    NSAttributedString.Key.foregroundColor: Colors.whiteText
                ]
                $0.largeTitleTextAttributes = [
                    NSAttributedString.Key.foregroundColor: Colors.whiteText
                ]
            }

            UINavigationBar.appearance().standardAppearance = navigationBarAppearance
        }

        UINavigationBar.appearance().tap {
            $0.tintColor = Colors.lightText
            $0.isTranslucent = true
            $0.barStyle = .default
            $0.backgroundColor = .clear
            $0.setBackgroundImage(UIImage(), for: .default)
            $0.shadowImage = UIImage()
            $0.prefersLargeTitles = true
            $0.titleTextAttributes = [
                NSAttributedString.Key.foregroundColor: Colors.whiteText
            ]
            $0.largeTitleTextAttributes = [
                NSAttributedString.Key.foregroundColor: Colors.whiteText
            ]
        }
    }

    // MARK: - Fonts

    static func font(ofSize size: CGFloat, weight: UIFont.Weight = .regular) -> UIFont {
        return UIFont.systemFont(ofSize: size, weight: weight)
    }

    // MARK: - Colors

    struct Colors {
        static let mainTint = UIColor("#351756")
        static let secondaryTint = UIColor("#FEEB34")
        static let alertRed = UIColor("#EA1E1E")

        static let whiteText = UIColor("#FFFFFF")
        static let lightText = UIColor("#A0A0A0")
        static let darkText = UIColor("#000000")

        static let whiteBackground = UIColor("#FFFFFF")
    }

    // MARK: - Styles

    struct Styles {
        static let mainView = UIViewStyle<UIView> {
            $0.backgroundColor = Colors.mainTint
        }

        static let contentView = UIViewStyle<UIView> {
            $0.backgroundColor = Appearance.Colors.whiteBackground
            $0.layer.cornerRadius = 16
            $0.layer.masksToBounds = true
        }

        static let oneLineAdjustableLabel = UIViewStyle<UILabel> {
            $0.numberOfLines = 1
            $0.adjustsFontSizeToFitWidth = true
            $0.minimumScaleFactor = 0.7
        }

        static let titleLabel = Styles.oneLineAdjustableLabel.composing {
            $0.textColor = Colors.whiteText
            $0.font = Appearance.font(ofSize: 32, weight: .semibold)
        }
    }
}
