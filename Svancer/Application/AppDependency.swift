//
//  AppDependency.swift
//  Svancer
//
//  Created by Jan Švancer on 05/11/2019.
//  Copyright © 2019 jansvancer. All rights reserved.
//

import UIKit

typealias AllDependencies = HasLoginService

protocol HasLoginService {
    var loginService: LoginService { get }
}

struct AppDependency: AllDependencies {
    let apiService: ApiService
    let loginService: LoginService

    init(application: UIApplication) {
        apiService = ApiService()
        loginService = LoginService(apiService: apiService)
    }
}
