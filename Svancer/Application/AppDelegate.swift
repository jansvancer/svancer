//
//  AppDelegate.swift
//  Svancer
//
//  Created by Jan Švancer on 05/11/2019.
//  Copyright © 2019 jansvancer. All rights reserved.
//

import UIKit
import RxSwift
import AlamofireNetworkActivityIndicator
import SwiftyBeaver

#if DEBUG
import AlamofireNetworkActivityLogger
#endif

let log = SwiftyBeaver.self

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let disposeBag = DisposeBag()
    private var appCoordinator: AppCoordinator!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        window = UIWindow(frame: UIScreen.main.bounds)

        // SwiftyBeaver
        let console = ConsoleDestination()  // log to Xcode Console
        console.format = "$DHH:mm:ss.SSS$d $C$L$c $N.$F:$l - $M"
        log.addDestination(console)

        // Global appearance
        Appearance.setGlobalAppearance()
        window?.tintColor = Appearance.Colors.mainTint

        // AppCoordinator
        appCoordinator = AppCoordinator(window: window!, application: application)
        appCoordinator.start()
            .subscribe()
            .disposed(by: disposeBag)

        NetworkActivityIndicatorManager.shared.isEnabled = true

        #if DEBUG
        NetworkActivityLogger.shared.startLogging()
        NetworkActivityLogger.shared.level = .debug
        #endif

        setupLoggingResources()

        return true
    }

    private func setupLoggingResources() {
        #if DEBUG
        Observable<Int>.interval(.seconds(1), scheduler: MainScheduler.instance)
            .map { _ in RxSwift.Resources.total }
            .distinctUntilChanged()
            .subscribe(onNext: { count in
                log.debug("RxSwift Resources count = \(count)")
            })
            .disposed(by: disposeBag)
        #endif
    }
}
