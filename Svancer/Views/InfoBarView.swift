//
//  InfoBarView.swift
//  Svancer
//
//  Created by Jan Švancer on 05/11/2019.
//  Copyright © 2019 jansvancer. All rights reserved.
//

import UIKit

final class InfoBarView: UIView {

    // MARK: - UI

    private let messageLabel = UILabel()
    private let panGestureRecognizer = UIPanGestureRecognizer()

    // MARK: - Properties

    var doAfter: (() -> Void)?
    private var originalPosition: CGPoint!
    private var draggingUp = false

    // MARK: - Initialization

    init() {
        super.init(frame: .zero)

        setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setupUI()
    }

    private func setupUI() {
        Styles.messageLabel.apply(to: messageLabel)

        addSubview(messageLabel)
        addGestureRecognizer(panGestureRecognizer)
        panGestureRecognizer.addTarget(self, action: #selector(dragging(_:)))

        messageLabel.snp.makeConstraints { make in
            make.leading.equalTo(self).offset(16)
            make.trailing.equalTo(self).offset(-16)
            make.centerY.equalTo(self.snp.centerY)
        }
    }

    // MARK: - Actions

    func show(message: String, offset: CGFloat = 0) {
        messageLabel.text = message

        self.frame.origin = CGPoint(x: 0, y: (-1 * self.frame.height))
        UIView.animate(withDuration: 0.4, animations: {
            self.frame.origin.y = 0
        }, completion: { _ in })
    }

    func hide() {
        UIView.animate(withDuration: 0.4, animations: {
            self.frame.origin.y -= self.frame.height
        }, completion: { _ in
            self.isHidden = true
            self.removeFromSuperview()
            self.doAfter?()
        })
    }

    @objc
    private func dragging(_ recognizer: UIPanGestureRecognizer) {
        if let view = recognizer.view {
            if recognizer.state == .began {
                originalPosition = view.center
            } else if recognizer.state == .changed {
                let translation = recognizer.translation(in: self)
                draggingUp = translation.y < 0    // store last direction
                var newY = view.center.y + translation.y
                if newY > originalPosition.y {
                    newY = originalPosition.y
                }
                view.center = CGPoint(x: view.center.x, y: newY)
                recognizer.setTranslation(CGPoint.zero, in: self)
            } else if recognizer.state == .ended {
                if draggingUp {
                    hide()
                } else {
                    // return view to original position (duration depends on distance from original position)
                    let duration = abs(originalPosition.y - view.center.y) / self.frame.height
                    recognizer.setTranslation(CGPoint.zero, in: self)
                    UIView.animate(withDuration: TimeInterval(duration)) {
                        view.center = self.originalPosition
                    }
                }
            }
        }
    }
}

// MARK: - Styles

extension InfoBarView {
    struct Styles {
        static let messageLabel = Appearance.Styles.oneLineAdjustableLabel.composing {
            $0.font = Appearance.font(ofSize: 14, weight: .medium)
            $0.textColor = Appearance.Colors.whiteText
            $0.textAlignment = .left
        }
    }
}

// MARK: - Create

extension InfoBarView {
    static func create() -> InfoBarView {
        return InfoBarView()
    }
}
