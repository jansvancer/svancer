//
//  Coordinator.swift
//  Svancer
//
//  Created by Jan Švancer on 05/11/2019.
//  Copyright © 2019 jansvancer. All rights reserved.
//

import Foundation
import RxSwift

enum PushCoordinationResult<T> {
    case popped
    case dismiss
    case finished(T)
}

enum ModalCoordinationResult<T> {
    case dismissed
    case dismiss
    case finished(T)
}

protocol Coordinator {
    associatedtype CoordinationResult

    var identifier: UUID { get }

    func start() -> Observable<CoordinationResult>
}
