//
//  ApiService.swift
//  Svancer
//
//  Created by Jan Švancer on 05/11/2019.
//  Copyright © 2019 jansvancer. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import RxAlamofire

// MARK: - Errors

enum ServerError: Error {
    case invalidResponse
    case clientError(code: Int, message: String)
    case invalidResponseCode(statusCode: Int)
    case timeout
    case invalidRequest
    case missingToken
}

// MARK: - ErrorPayload

struct ErrorPayload: Decodable {
    var success: Bool
    var error: Error

    struct Error: Decodable {
        var code: Int?
        var message: String?
    }
}

// MARK: - ApiService

final class ApiService {

    struct StatusCode {
        static let ok = 200
        static let created = 201
        static let accepted = 202
        static let unauthorized = 401
        static let notFound = 404

        static let success = 200...299
        static let clientError = 400...499
        static let serverError = 500...599

        static let valid = 200...499
    }

    static let jsonContentType = "application/json"
    private let decoder: JSONDecoder

    let sessionManager: SessionManager

    init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60
        configuration.timeoutIntervalForResource = 60

        self.sessionManager = Alamofire.SessionManager(configuration: configuration)
        self.sessionManager.adapter = ApiAdapter()

        self.decoder = Constants.jsonDecoder
    }

    // MARK: - Requests

    func request(endpoint: ApiRouter) -> Observable<Data> {
        return Observable.create { [unowned self] observer in
            let request = self.sessionManager.request(endpoint)
                .validate()
                .validate(contentType: [ApiService.jsonContentType])
                .responseData { response in
                    do {
                        try self.validateResponse(response: response.response, data: response.data)
                        guard let data = response.data else {
                            throw ServerError.invalidResponse
                        }

                        observer.onNext(data)
                        observer.onCompleted()
                    } catch {
                        observer.onError(error)
                    }
                }

            return Disposables.create {
                request.cancel()
            }
        }
    }

    func voidRequest(endpoint: ApiRouter) -> Observable<Void> {
        return Observable.create { [unowned self] observer in
            let request = self.sessionManager.request(endpoint)
                .validate()
                .validate(contentType: [ApiService.jsonContentType])
                .responseData { response in
                    do {
                        try self.validateResponse(response: response.response, data: response.data)
                        observer.onNext(())
                        observer.onCompleted()
                    } catch {
                        observer.onError(error)
                    }
                }

            return Disposables.create {
                request.cancel()
            }
        }
    }

    // MARK: - Helpers

    private func validateResponse(response: URLResponse?, data: Data?) throws {
        guard let httpResponse = response as? HTTPURLResponse else {
            throw ServerError.invalidResponse
        }

        if !(ApiService.StatusCode.valid ~= httpResponse.statusCode) {
            throw ServerError.invalidResponseCode(statusCode: httpResponse.statusCode)
        }

        if !(ApiService.StatusCode.success ~= httpResponse.statusCode),
            let data = data,
            let errorPayload = try? decoder.decode(ErrorPayload.self, from: data) {
            throw ServerError.clientError(code: errorPayload.error.code ?? 0, message: errorPayload.error.message ?? "")
        }
    }
}
