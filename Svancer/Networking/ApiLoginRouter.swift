//
//  ApiLoginRouter.swift
//  Svancer
//
//  Created by Jan Švancer on 05/11/2019.
//  Copyright © 2019 jansvancer. All rights reserved.
//

import Foundation
import Alamofire

enum ApiLoginRouter: ApiRouter {
    case login(username: String, passwordHash: String)

    // MARK: - Method

    var method: HTTPMethod {
        switch self {
        case .login:
            return .post
        }
    }

    // MARK: - Path

    var path: String {
        switch self {
        case .login:
            return "/download/bootcamp/image.php"
        }
    }

    // MARK: - Parameters

    var parameters: Parameters {
        switch self {
        default:
            return [:]
        }
    }

    // MARK: - URLRequestConvertible

    func asURLRequest() throws -> URLRequest {
        let urlPath = "\(Constants.API.baseURLString)\(path)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        var urlRequest = URLRequest(url: try urlPath.asURL())

        urlRequest.httpMethod = method.rawValue

        switch self {
        case .login(let username, let passwordHash):
            urlRequest.httpBody = "username=\(username)".data(using: String.Encoding.utf8)
            urlRequest.setValue(passwordHash, forHTTPHeaderField: "Authorization")
        }

        return urlRequest
    }
}
