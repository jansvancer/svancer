//
//  LoginImage.swift
//  Svancer
//
//  Created by Jan Švancer on 05/11/2019.
//  Copyright © 2019 jansvancer. All rights reserved.
//

import Foundation

struct LoginImage: Codable {
    let image: String
}
