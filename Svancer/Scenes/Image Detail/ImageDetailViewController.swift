//
//  ImageDetailViewController.swift
//  Svancer
//
//  Created by Jan Švancer on 05/11/2019.
//  Copyright © 2019 jansvancer. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class ImageDetailViewController: RxViewController, ViewModelAttaching {

    var viewModel: ImageDetailViewModel!
    var bindings: ImageDetailViewModel.Bindings {
        return ImageDetailViewModel.Bindings(
            didMoveToParent: rx.sentMessage(#selector(UIViewController.didMove)).asDriverOnErrorJustReturnNil(),
            backTap: backBarButtonItem.rx.tap.asDriver()
        )
    }

    // MARK: - UI

    private let backBarButtonItem = UIBarButtonItem(image: R.image.icArrowBack(), style: .plain, target: nil, action: nil)
    private let imageView = UIImageView()

    // MARK: - Properties

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func setupUI() {
        Appearance.Styles.mainView.apply(to: view)
        Appearance.Styles.contentView.apply(to: imageView)
        Styles.imageView.apply(to: imageView)
    }

    func arrangeSubviews() {
        view.addSubview(imageView)
    }

    func layout() {
        imageView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.top.equalTo(view.safeArea.top).offset(48)
            make.bottom.equalTo(view.safeArea.bottom).offset(-96)
        }
    }

    func setupNavigationBar() {
        title = tr(L.imageDetailTitleLabel)
        navigationItem.leftBarButtonItem = backBarButtonItem
    }

    override func setupVisibleBindings(for visibleDisposeBag: DisposeBag) {
        super.setupVisibleBindings(for: visibleDisposeBag)

        imageView.image = viewModel.image
    }

}

// MARK: - Styles

extension ImageDetailViewController {
    struct Styles {
        static let imageView = UIViewStyle<UIImageView> {
            $0.contentMode = .scaleAspectFit
        }
    }
}

// MARK: - Create

extension ImageDetailViewController {
    static func create() -> ImageDetailViewController {
        return ImageDetailViewController()
    }
}
