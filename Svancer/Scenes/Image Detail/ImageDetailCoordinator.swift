//
//  ImageDetailCoordinator.swift
//  Svancer
//
//  Created by Jan Švancer on 05/11/2019.
//  Copyright © 2019 jansvancer. All rights reserved.
//

import UIKit
import RxSwift

final class ImageDetailCoordinator: BaseCoordinator<PushCoordinationResult<Void>> {

    typealias Dependencies = AllDependencies

    private let navigationController: UINavigationController
    private let dependencies: Dependencies
    private let loginImage: LoginImage

    init(navigationController: UINavigationController, dependencies: Dependencies, loginImage: LoginImage) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.loginImage = loginImage
    }

    override func start() -> Observable<CoordinationResult> {
        let viewController = ImageDetailViewController.create()
        let viewModel = viewController.attach(wrapper: ViewModelWrapper<ImageDetailViewModel>(ImageDetailViewModel.Dependency(
            loginImage: loginImage
        )))

        navigationController.pushViewController(viewController, animated: true)

        let popped = viewModel.viewDismissed
            .asObservable()
            .map { _ in PushCoordinationResult<Void>.popped }

        let cancel = viewModel.cancelTapped
            .asObservable()
            .map { _ in PushCoordinationResult<Void>.dismiss }

        return Observable.merge(popped, cancel)
            .take(1)
            .do(onNext: { [weak self] in
                if case .dismiss = $0 {
                    self?.navigationController.popViewController(animated: true)
                }
            })
    }
}
