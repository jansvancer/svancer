//
//  ImageDetailViewModel.swift
//  Svancer
//
//  Created by Jan Švancer on 05/11/2019.
//  Copyright © 2019 jansvancer. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class ImageDetailViewModel: ViewModelType {

    // MARK: - ViewModelType

    struct Dependency {
        let loginImage: LoginImage
    }

    struct Bindings {
        let didMoveToParent: Driver<[Any]?>
        let backTap: Driver<Void>
    }

    // MARK: - Variables

    private(set) var image: UIImage?

    // MARK: - Actions

    private(set) var viewDismissed: Driver<Void>!
    private(set) var cancelTapped: Driver<Void>!

    // MARK: - Initialization

    init(dependency: Dependency, bindings: Bindings) {
        setupDataUpdates(dependency: dependency, bindings: bindings)
        setupActions(dependency: dependency, bindings: bindings)
    }

    private func setupDataUpdates(dependency: Dependency, bindings: Bindings) {
        image = createImageFromBase64(base64: dependency.loginImage.image)
    }

    private func setupActions(dependency: Dependency, bindings: Bindings) {
        viewDismissed = bindings.didMoveToParent
            .filter { ($0?.first as? NSNull) != nil }
            .asVoid

        cancelTapped = bindings.backTap
    }
}

// MARK: - Create image

extension ImageDetailViewModel {
    private func createImageFromBase64(base64: String) -> UIImage? {
        guard let dataDecoded = Data(base64Encoded: base64, options: .ignoreUnknownCharacters) else {
            return nil
        }

        return UIImage(data: dataDecoded)
    }
}
