//
//  LoginCoordinator.swift
//  Svancer
//
//  Created by Jan Švancer on 05/11/2019.
//  Copyright © 2019 jansvancer. All rights reserved.
//

import UIKit
import RxSwift

final class LoginCoordinator: BaseCoordinator<Void> {

    typealias Dependencies = AllDependencies

    private let window: UIWindow
    private let dependencies: Dependencies

    init(window: UIWindow, dependencies: Dependencies) {
        self.window = window
        self.dependencies = dependencies
    }

    override func start() -> Observable<CoordinationResult> {
        let viewController = LoginViewController.create()
        let navigationController = SwipeNavigationController(rootViewController: viewController)
        let viewModel = viewController.attach(wrapper: ViewModelWrapper<LoginViewModel>(dependencies))

        window.tap {
            $0.rootViewController = navigationController
            $0.makeKeyAndVisible()
        }

        viewModel.loggedInImage
            .asObservable()
            .flatMap { [weak self, weak navigationController] loginImage -> Observable<PushCoordinationResult<Void>> in
                guard let self = self, let navigationController = navigationController else {
                    return .empty()
                }

                return self.showImageDetail(loginImage: loginImage, on: navigationController)
            }
            .subscribe()
            .disposed(by: disposeBag)

        return Observable.never()
    }

    private func showImageDetail(loginImage: LoginImage,
                                 on navigationController: UINavigationController) -> Observable<PushCoordinationResult<Void>> {
        let coordinator = ImageDetailCoordinator(navigationController: navigationController,
                                                 dependencies: dependencies,
                                                 loginImage: loginImage)
        return coordinate(to: coordinator)
    }
}
