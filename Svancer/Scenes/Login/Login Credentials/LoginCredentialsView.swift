//
//  LoginCredentialsView.swift
//  Svancer
//
//  Created by Jan Švancer on 06/11/2019.
//  Copyright © 2019 jansvancer. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import JVFloatLabeledTextField

final class LoginCredentialsView: UIStackView {

    // MARK: - UI

    private let usernameTextField = JVFloatLabeledTextField()
    private let passwordTextField = JVFloatLabeledTextField()

    // MARK: - Properties

    let disposeBag = DisposeBag()
    private(set) var usernameValue: Driver<String>!
    private(set) var passwordValue: Driver<String>!
    private(set) var sendTap: Driver<Void>!

    // MARK: - Initialization

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupUI()
        setupBindings()
    }

    required init(coder: NSCoder) {
        super.init(coder: coder)

        setupUI()
        setupBindings()
    }

    private func setupUI() {
        usernameTextField.placeholder = tr(L.loginUsernameTextField)
        passwordTextField.placeholder = tr(L.loginPasswordTextField)

        Styles.stackView.apply(to: self)
        Styles.textField.apply(to: [usernameTextField, passwordTextField])
        Styles.nextTextField.apply(to: usernameTextField)
        Styles.sendTextField.apply(to: passwordTextField)
        Styles.passwordTextField.apply(to: passwordTextField)

        arrangeSubviews()
        layout()
    }

    private func arrangeSubviews() {
        addArrangedSubview(usernameTextField)
        addArrangedSubview(passwordTextField)
    }

    private func layout() {
        arrangedSubviews.forEach {
            $0.snp.makeConstraints { make in
                make.height.equalTo(56)
            }
        }
    }

    private func setupBindings() {
        usernameValue = usernameTextField.rx.text.orEmpty.asDriver()
        passwordValue = passwordTextField.rx.text.orEmpty.asDriver()
        sendTap = passwordTextField.rx.controlEvent(.editingDidEndOnExit).asDriver()

        usernameTextField.rx.controlEvent(.editingDidEndOnExit).asDriver()
            .drive(onNext: { [weak self] _ in
                self?.passwordTextField.becomeFirstResponder()
            })
            .disposed(by: disposeBag)
    }
}

// MARK: - Update

extension LoginCredentialsView {
    func startFocus() {
        usernameTextField.becomeFirstResponder()
        usernameTextField.text = nil
        passwordTextField.text = nil
    }
}

// MARK: - Styles

extension LoginCredentialsView {
    struct Styles {
        static let stackView = UIViewStyle<UIStackView> {
            $0.axis = .vertical
            $0.distribution = .fill
            $0.alignment = .fill
        }

        static let textField = UIViewStyle<JVFloatLabeledTextField> {
            $0.placeholderColor = Appearance.Colors.lightText
            $0.floatingLabelTextColor = Appearance.Colors.lightText
            $0.floatingLabelActiveTextColor = Appearance.Colors.mainTint
            $0.floatingLabelFont = Appearance.font(ofSize: 14, weight: .regular)
            $0.textColor = Appearance.Colors.darkText
            $0.floatingLabelYPadding = 4
            $0.clipsToBounds = true
            $0.autocapitalizationType = .none
        }

        static let nextTextField = UIViewStyle<UITextField> {
            $0.returnKeyType = .next
        }

        static let sendTextField = UIViewStyle<UITextField> {
            $0.returnKeyType = .send
        }

        static let passwordTextField = UIViewStyle<UITextField> {
            $0.isSecureTextEntry = true
        }
    }
}
