//
//  LoginViewController.swift
//  Svancer
//
//  Created by Jan Švancer on 05/11/2019.
//  Copyright © 2019 jansvancer. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxKeyboard
import SnapKit

final class LoginViewController: RxViewController, ViewModelAttaching {

    var viewModel: LoginViewModel!
    var bindings: LoginViewModel.Bindings {
        return LoginViewModel.Bindings(
            username: loginCredentialsView.usernameValue,
            password: loginCredentialsView.passwordValue,
            loginTap: Driver.merge(
                loginButton.rx.tap.asDriver(),
                loginCredentialsView.sendTap
            )
        )
    }

    // MARK: - UI

    private let loginView = UIView()
    private let loginCredentialsView = LoginCredentialsView()
    private let loginButton = UIButton()
    private let tapGestureRecognizer = UITapGestureRecognizer()

    // MARK: - Properties

    private var loginViewBottomConstraint: Constraint!

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func setupUI() {
        Appearance.Styles.mainView.apply(to: view)
        Appearance.Styles.contentView.apply(to: loginView)
        Styles.loginButton.apply(to: loginButton)

        loginButton.setTitle(tr(L.loginLoginButton).uppercased(), for: .normal)
    }

    func arrangeSubviews() {
        loginView.addSubview(loginCredentialsView)
        view.addSubview(loginView)
        view.addSubview(loginButton)

        view.addGestureRecognizer(tapGestureRecognizer)
    }

    func layout() {
        loginView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.top.equalTo(view.safeArea.top).offset(32)
            loginViewBottomConstraint = make.bottom.equalTo(view.safeArea.bottom).offset(-96).constraint
        }

        loginCredentialsView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.top.equalToSuperview().offset(16)
            make.bottom.lessThanOrEqualToSuperview().offset(-16)
        }

        loginButton.snp.makeConstraints { make in
            make.leading.greaterThanOrEqualTo(loginView.snp.leading)
            make.trailing.lessThanOrEqualTo(loginView.snp.trailing)
            make.centerY.equalTo(loginView.snp.bottom)
            make.centerX.equalTo(loginView.snp.centerX)
            make.height.equalTo(56)
        }
    }

    func setupNavigationBar() {
        title = tr(L.loginTitleLabel)
    }

    override func setupVisibleBindings(for visibleDisposeBag: DisposeBag) {
        super.setupVisibleBindings(for: visibleDisposeBag)

        tapGestureRecognizer.rx.event.asDriver()
            .drive(onNext: { [unowned self] _ in
                self.view.endEditing(true)
            })
            .disposed(by: visibleDisposeBag)

        RxKeyboard.instance.visibleHeight
            .skip(1)
            .drive(onNext: { [unowned self] keyboardVisibleHeight in
                let offset = keyboardVisibleHeight.isZero ? -96 : -keyboardVisibleHeight + self.view.safeAreaInsets.bottom - 32
                self.loginViewBottomConstraint.update(offset: offset)

                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            })
            .disposed(by: visibleDisposeBag)

        rx.sentMessage(#selector(UIViewController.viewDidAppear(_:))).asVoid.asDriverFilterNil()
            .drive(onNext: { [unowned self] _ in
                self.loginCredentialsView.startFocus()
            })
            .disposed(by: visibleDisposeBag)

        viewModel.loading
            .drive(self.rx.activityIndicator)
            .disposed(by: visibleDisposeBag)

        viewModel.errors
            .drive(self.rx.errors)
            .disposed(by: visibleDisposeBag)

        viewModel.loginEnabled
            .drive(loginButton.rx.isEnabled)
            .disposed(by: visibleDisposeBag)
    }
}

// MARK: - Styles

extension LoginViewController {
    struct Styles {
        static let loginButton = UIViewStyle<UIButton> {
            $0.backgroundColor = Appearance.Colors.secondaryTint
            $0.setTitleColor(Appearance.Colors.mainTint, for: .normal)
            $0.titleLabel?.font = Appearance.font(ofSize: 20)
            $0.contentEdgeInsets = UIEdgeInsets(top: 0, left: 32, bottom: 0, right: 32)
            $0.layer.cornerRadius = 28
            $0.layer.masksToBounds = true
        }
    }
}

// MARK: - Create

extension LoginViewController {
    static func create() -> LoginViewController {
        return LoginViewController()
    }
}
