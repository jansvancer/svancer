//
//  LoginViewModel.swift
//  Svancer
//
//  Created by Jan Švancer on 05/11/2019.
//  Copyright © 2019 jansvancer. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class LoginViewModel: ViewModelType {

    // MARK: - ViewModelType

    typealias Dependency = HasLoginService

    struct Bindings {
        let username: Driver<String>
        let password: Driver<String>
        let loginTap: Driver<Void>
    }

    // MARK: - Variables

    let loading: Driver<Bool>
    let errors: Driver<Error>
    private(set) var loginEnabled: Driver<Bool>!

    private let activityIndicator = RxActivityIndicator()
    private let errorTracker = RxErrorTracker()

    // MARK: - Actions

    private(set) var loggedInImage: Driver<LoginImage>!

    // MARK: - Initialization

    init(dependency: Dependency, bindings: Bindings) {
        loading = activityIndicator.asDriver()
        errors = errorTracker.asDriver()

        setupDataUpdates(dependency: dependency, bindings: bindings)
        setupActions(dependency: dependency, bindings: bindings)
    }

    private func setupDataUpdates(dependency: Dependency, bindings: Bindings) {
        loginEnabled = Driver.combineLatest(bindings.username, bindings.password)
            .map { username, password in
                 !username.isEmpty && !password.isEmpty
            }
    }

    private func setupActions(dependency: Dependency, bindings: Bindings) {
         let formInputs = Driver.combineLatest(bindings.username, bindings.password)

        loggedInImage = bindings.loginTap
            .withLatestFrom(loginEnabled)
            .filter { $0 }
            .withLatestFrom(formInputs)
            .flatMapLatest { [unowned self] username, password in
                dependency.loginService.login(username: username, password: password)
                    .trackActivity(self.activityIndicator)
                    .trackError(self.errorTracker)
                    .asDriverFilterNil()
            }
    }
}
