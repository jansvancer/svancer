//
//  String+Extensions.swift
//  Svancer
//
//  Created by Jan Švancer on 05/11/2019.
//  Copyright © 2019 jansvancer. All rights reserved.
//

import Foundation

extension String {

    func removeWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
}
