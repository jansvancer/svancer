//
//  UIView+SnapKit.swift
//  Svancer
//
//  Created by Jan Švancer on 05/11/2019.
//  Copyright © 2019 jansvancer. All rights reserved.
//

import UIKit
import SnapKit

extension UIView {

    var safeArea: ConstraintBasicAttributesDSL {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.snp
        }

        return self.snp
    }
}
