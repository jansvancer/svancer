//
//  UserDefaults+Extensions.swift
//  Svancer
//
//  Created by Jan Švancer on 05/11/2019.
//  Copyright © 2019 jansvancer. All rights reserved.
//

import Foundation

extension UserDefaults {

    func set<T>(_ value: T, for constant: Constants.UserDefaults) {
        set(value, forKey: constant.rawValue)
        synchronize()
    }

    func getOptional<T>(for constant: Constants.UserDefaults) -> T? {
        return object(forKey: constant.rawValue) as? T
    }

    func remove(for constant: Constants.UserDefaults) {
        removeObject(forKey: constant.rawValue)
        synchronize()
    }

    func removeAll() {
        dictionaryRepresentation().keys.forEach { removeObject(forKey: $0) }
        synchronize()
    }
}
