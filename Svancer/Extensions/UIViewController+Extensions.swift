//
//  UIViewController+Extensions.swift
//  Svancer
//
//  Created by Jan Švancer on 05/11/2019.
//  Copyright © 2019 jansvancer. All rights reserved.
//

import UIKit

// MARK: - Show message

extension UIViewController {
    func showMessage(_ message: String, backgroundColor: UIColor = Appearance.Colors.alertRed) {
        let barView = InfoBarView.create()
        let height = UIApplication.shared.statusBarFrame.height + 44
        barView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: height)
        barView.backgroundColor = backgroundColor
        var alertWindow: UIWindow! = UIWindow(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: height))
        alertWindow.backgroundColor = UIColor.clear
        alertWindow.windowLevel = UIWindow.Level.alert
        alertWindow.isHidden = false
        alertWindow.makeKeyAndVisible()
        alertWindow.addSubview(barView)
        barView.doAfter = {
            if let window = alertWindow {
                window.isHidden = true
                alertWindow = nil
            }
        }
        barView.show(message: message.uppercased())
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
            barView.hide()
        }
    }

    func handleError(error: Error) {
        if let urlError = error as? URLError, urlError.code == .notConnectedToInternet {
            showMessage(tr(L.errorConnection), backgroundColor: Appearance.Colors.alertRed)
        } else if let serverError = error as? ServerError {
            switch serverError {
            case .clientError(_, let message):
                showMessage(message, backgroundColor: Appearance.Colors.alertRed)
            case .invalidResponse, .invalidRequest, .invalidResponseCode:
                showMessage(tr(L.errorServer), backgroundColor: Appearance.Colors.alertRed)
            case .timeout:
                showMessage(tr(L.errorTimeout), backgroundColor: Appearance.Colors.alertRed)
            case .missingToken:
                showMessage(tr(L.errorMissingToken), backgroundColor: Appearance.Colors.alertRed)
            }
        } else {
            showMessage(tr(L.errorUnknown), backgroundColor: Appearance.Colors.alertRed)
        }
    }
}

// MARK: - Activity indicator

extension UIViewController {
    var activityIndicatorTag: Int { return 999_999 }
    var activityBackgroundViewTag: Int { return 999_995 }

    func startActivity(aboveNavigationController: Bool = false) {
        var view: UIView = self.view
        view.endEditing(true)

        if aboveNavigationController, let navigationView = self.navigationController?.view {
            view = navigationView
        }

        if self.navigationController?.view.viewWithTag(activityBackgroundViewTag) != nil {
            self.stopActivity()
        }

        if self.view.viewWithTag(activityBackgroundViewTag) != nil {
            self.stopActivity()
        }

        DispatchQueue.main.async(execute: {
            // Transparent black view cover
            let activityBackgroundView = UIView(frame: CGRect.zero)
            activityBackgroundView.tag = self.activityBackgroundViewTag
            activityBackgroundView.alpha = 0.5
            activityBackgroundView.backgroundColor = UIColor.lightGray
            view.addSubview(activityBackgroundView)

            activityBackgroundView.snp.makeConstraints { make in
                make.leading.equalTo(view)
                make.trailing.equalTo(view)
                make.top.equalTo(view)
                make.bottom.equalTo(view)
            }

            // Loading spinner
            let activityIndicator = UIActivityIndicatorView(style: .gray)
            activityIndicator.startAnimating()
            activityIndicator.tag = self.activityIndicatorTag
            activityBackgroundView.addSubview(activityIndicator)

            activityIndicator.snp.makeConstraints { make in
                make.center.equalTo(activityBackgroundView.snp.center)
            }
        })
    }

    func stopActivity(completion: (() -> Void)? = nil) {
        DispatchQueue.main.async(execute: {
            if let activityBackgroundView = self.view.subviews.first(where: { $0.tag == self.activityBackgroundViewTag }) {
                UIView.animate(withDuration: 0.2, animations: {
                    activityBackgroundView.alpha = 0.0
                }, completion: { finished in
                    if finished {
                        activityBackgroundView.removeFromSuperview()
                    }
                })
            }
        })

        DispatchQueue.main.async(execute: {
            if let activityBackgroundView = self.navigationController?.view.subviews.first(where: { $0.tag == self.activityBackgroundViewTag }) {
                UIView.animate(withDuration: 0.2, animations: {
                    activityBackgroundView.alpha = 0.0
                }, completion: { finished in
                    if finished {
                        activityBackgroundView.removeFromSuperview()
                    }
                })
            }
        })

        DispatchQueue.main.async(execute: {
            if let loadingImageView = self.view.subviews.first(where: { $0.tag == self.activityIndicatorTag }) as? UIImageView {
                UIView.animate(withDuration: 0.2, animations: {
                    loadingImageView.alpha = 0.0
                }, completion: { finished in
                    if finished {
                        loadingImageView.removeFromSuperview()
                        if let completion = completion {
                            completion()
                        }
                    }
                })
            }

            if let loadingImageView = self.navigationController?.view.subviews.first(where: { $0.tag == self.activityIndicatorTag }) as? UIImageView {
                UIView.animate(withDuration: 0.2, animations: {
                    loadingImageView.alpha = 0.0
                }, completion: { finished in
                    if finished {
                        loadingImageView.removeFromSuperview()
                        if let completion = completion {
                            completion()
                        }
                    }
                })
            }
        })
    }
}

