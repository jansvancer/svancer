//
//  LoginService.swift
//  Svancer
//
//  Created by Jan Švancer on 05/11/2019.
//  Copyright © 2019 jansvancer. All rights reserved.
//

import Foundation
import RxSwift

final class LoginService: BaseService {}

// MARK: - LoginServiceType

protocol LoginServiceType {
    func login(username: String, password: String) -> Single<LoginImage>
}

extension LoginService: LoginServiceType {
    func login(username: String, password: String) -> Single<LoginImage> {
        return request(type: LoginImage.self, endpoint: ApiLoginRouter.login(username: username,
                                                                             passwordHash: password.sha1()))
    }
}
