//
//  BaseService.swift
//  Svancer
//
//  Created by Jan Švancer on 05/11/2019.
//  Copyright © 2019 jansvancer. All rights reserved.
//

import Foundation
import RxSwift

enum ServiceError: Error {
    case serializationError(message: String)
}

class BaseService {

    let apiService: ApiService
    let disposeBag = DisposeBag()

    init(apiService: ApiService) {
        self.apiService = apiService
    }

    // MARK: - Requests

    func request<T: Decodable>(type: T.Type, endpoint: ApiRouter, scheduler: ConcurrentDispatchQueueScheduler? = nil) -> Single<T> {
        var request = apiService.request(endpoint: endpoint)

        if let scheduler = scheduler {
            request = request.observeOn(scheduler)
        }

        return request
            .map { [unowned self] data -> T in
                try self.serialize(data: data, toObject: T.self)
            }
            .asSingle()
    }

    func request(endpoint: ApiRouter) -> Single<Void> {
        return apiService.voidRequest(endpoint: endpoint)
            .asSingle()
    }

    // MARK: - Serializer

    private func serialize<T: Decodable>(data: Data, toObject: T.Type) throws -> T {
        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
        guard let jsonDict = json as? [String: Any] else {
            throw ServiceError.serializationError(message: "Failed to parse JSON")
        }

        if jsonDict.keys.contains("data") {
            return try T(data: data, keyPath: "data")
        }

        return try T(data: data)
    }
}
