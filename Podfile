platform :ios, '11.0'

inhibit_all_warnings!

target 'Svancer' do

    use_frameworks!

    # HockeyApp
    pod 'HockeySDK'

    # Strong typing
    pod 'R.swift'

    # Swift syntax control
    pod 'SwiftLint'

    # Keychain
    pod 'KeychainAccess'

    # Reactive programming
    pod 'RxSwift'
    pod 'RxCocoa'
    pod 'RxKeyboard'

    # Networking
    pod 'Alamofire'
    pod 'RxAlamofire'
    pod 'AlamofireNetworkActivityIndicator'
    pod 'AlamofireNetworkActivityLogger', configuration: ['Debug']

    # Crashlytics 
    pod 'Fabric'
    pod 'Crashlytics'

    # Logging
    pod 'SwiftyBeaver'

    # UI
    pod 'SnapKit'
    pod 'JVFloatLabeledTextField'

    target 'SvancerTests' do
    end

end

post_install do |installer|
    installer.pods_project.targets.each do |target|
        if target.name == 'RxSwift'
            target.build_configurations.each do |config|
                if config.name == 'Debug'
                    config.build_settings['OTHER_SWIFT_FLAGS'] ||= ['-D', 'TRACE_RESOURCES']
                end
            end
        end

        target.build_configurations.each do |config|
            if config.build_settings['IPHONEOS_DEPLOYMENT_TARGET'].to_f < 8.0
                config.build_settings['IPHONEOS_DEPLOYMENT_TARGET'] = '8.0'
            end
        end
    end

    installer.pods_project.build_configurations.each do |config|
        config.build_settings.delete('CODE_SIGNING_ALLOWED')
        config.build_settings.delete('CODE_SIGNING_REQUIRED')
    end
end
